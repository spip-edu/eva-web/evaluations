<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_evaluations_critique'         => 'Ajouter cette critique',

	// I
	'icone_creer_evaluations_critique'          => 'Créer une critique',
	'icone_modifier_evaluations_critique'       => 'Modifier cette critique',
	'info_1_evaluations_critique'               => 'Une critique',
	'info_aucun_evaluations_critique'           => 'Aucune critique',
	'info_evaluations_critiques_auteur'         => 'Les critiques de cet auteur',
	'info_nb_evaluations_critiques'             => '@nb@ critiques',

	// L
	'label_commentaire'                         => 'Commentaire',
	'label_commentaires'                        => 'Commentaires',
	'label_evaluations_critique'                => 'Type d\'objet',
	'label_faiblesses'                          => 'Faiblesses',
	'label_forces'                              => 'Forces',
	'label_id_auteur'                           => 'Auteur',
	'label_id_evaluation'                       => 'Évaluation',
	'label_id_evaluations_critere'              => 'Critère d\'évaluation',
	'label_id_evaluations_critique'             => 'Identifiant d\'objet',
	'label_note'                                => 'Note',

	// R
	'retirer_lien_evaluations_critique'         => 'Retirer cette critique',
	'retirer_tous_liens_evaluations_critiques'  => 'Retirer toutes les critiques',

	// T
	'texte_ajouter_evaluations_critique'        => 'Ajouter une critique',
	'texte_changer_statut_evaluations_critique' => 'Cette critique est :',
	'texte_creer_associer_evaluations_critique' => 'Créer et associer une critique',
	'titre_evaluations_critique'                => 'Critique',
	'titre_evaluations_critiques'               => 'Critiques',
	'titre_evaluations_critiques_rubrique'      => 'Critiques de la rubrique',
	'titre_langue_evaluations_critique'         => 'Langue de cette critique',
	'titre_logo_evaluations_critique'           => 'Logo de cette critique',
);

?>

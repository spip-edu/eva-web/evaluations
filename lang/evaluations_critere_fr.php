<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(

    // A
    'ajouter_lien_evaluations_critere'         => 'Ajouter ce critère d\'évaluation',

    // C
    'confirmer_supprimer_evaluations_critere'  => 'La suppression de ce critère entraînera également la suppression
        de toutes les critiques qui y étaient associées. Êtes-vous sûr ?',

    // E
    'explication_rang'                         => 'Pour ordonner les critères. Indiquez un numéro de rang.',
    'explication_ponderation'                  => "Pondère ce critère par rapport à l'évaluation globale",

    // F
    'fieldset_noter'                           => 'Note',
    'fieldset_commenter'                       => 'Commentaire',
    'fieldset_evaluer'                         => 'Forces & faiblesses',

    // I
    'icone_creer_evaluations_critere'          => 'Créer un critère d\'évaluation',
    'icone_modifier_evaluations_critere'       => 'Modifier ce critère d\'évaluation',
    'info_1_evaluations_critere'               => 'Un critère d\'évaluation',
    'info_aucun_evaluations_critere'           => 'Aucun critère d\'évaluation',
    'info_evaluations_criteres_auteur'         => 'Les critères d\'évaluation de cet auteur',
    'info_nb_evaluations_criteres'             => '@nb@ critères d\'évaluation',
    'info_nouveau_evaluations_critere'         => 'Nouveau critère d\'évaluation',

    // L
    'label_aide_noter'                         => 'Texte d\'aide pour la note',
    'label_commenter'                          => 'Commenter',
    'label_aide_commenter'                     => 'Texte d\'aide pour le commentaire',
    'label_evaluer'                            => 'Évaluer les forces et faiblesses',
    'label_aide_evaluer'                       => 'Texte d\'aide pour les forces et faiblesses',
    'label_evaluation'                         => 'Évaluer',
    'label_id_evaluation'                      => 'Évaluation',
    'label_note_maxi'                          => 'Note maximale',
    'label_note_mini'                          => 'Note minimale',
    'label_noter'                              => 'Noter',
    'label_note'                               => 'Note',
    'label_ponderation'                        => 'Poids',
    'label_rang'                               => 'Rang',
    'label_texte'                              => "Quelques mots d'explication sur le critère",
    'label_titre'                              => "Titre",

    // R
    'retirer_lien_evaluations_critere'         => 'Retirer ce critère d\'évaluation',
    'retirer_tous_liens_evaluations_criteres'  => 'Retirer tous les critères d\'évaluation',

    // S
    'supprimer_evaluations_critere'            => 'Supprimer ce critère d\'évaluation',
    
    // T
    'texte_ajouter_evaluations_critere'        => 'Ajouter un critère d\'évaluation',
    'texte_changer_statut_evaluations_critere' => 'Ce critère d\'évaluation est :',
    'texte_creer_associer_evaluations_critere' => 'Créer et associer un critère d\'évaluation',
    'titre_evaluations_critere'                => 'Critère d\'évaluation',
    'titre_evaluations_criteres'               => 'Critères d\'évaluation',
    'titre_evaluations_criteres_rubrique'      => 'Critères d\'évaluation de la rubrique',
    'titre_langue_evaluations_critere'         => 'Langue de ce critère d\'évaluation',
    'titre_logo_evaluations_critere'           => 'Logo de ce critère d\'évaluation',
);

?>

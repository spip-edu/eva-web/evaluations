<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_sortie'                   => "Retourner au site principal",

	// E
	'evaluations_titre'               => 'Évaluations',

	// C
	'cfg_exemple'                     => 'Exemple',
	'cfg_exemple_explication'         => 'Explication de cet exemple',
	'cfg_titre_parametrages'          => 'Paramétrages',

	// I
	'info_evaluation_deja_effectuee'  => "Vous avez déjà répondu à ce formulaire le @date_critique@.<br />Vous pouvez modifier ou compléter votre saisie.",
	'info_reponses_mises_a_jour'      => "Vos réponses ont été mises à jour",
	'info_evaluation_prise_en_compte' => "Merci pour votre évaluation",

	// L
	'label_votre_commentaire'           => "Votre commentaire :",
	'label_votre_note'                  => "Note : ",
	'label_points_forts'                => "Points forts : ",
	'label_points_faibles'              => "À améliorer  : ",
	'liste_objets_critiques'            => "Liste des objets ayant reçu des évaluations",
	'liste_auteurs_critiques'           => "Liste des auteurs ayant évalué au moins un élément",

	// S
	'selectionner_evaluation'           => "Sélectionner l'évaluation à utiliser",

	// T
	'titre_page_configurer_evaluations' => 'Configurer les évalutations',
	'titre_bloc_critiques'              => "Les critiques pour le @objet@ N°@id_objet@ «@titre@», dans le cadre de «@evaluation@»",
	'titre_visualiser_evaluations'      => "Visualisation des évaluations transmises",
	'titre_critere_evaluation'          => "Criteres de l'évaluation"

	// V

);

?>

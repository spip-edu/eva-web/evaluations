<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'evaluations_description' => 'Évaluation fournit un processus assez simple pour noter, évaluer ou critiquer des éléments (par exemple des articles, documents, sites) par des visiteurs identifiés sur le site.

Les évaluations récoltées peuvent vous aider à prendre des décisions, à créer des moyennes des notes, élire des gagnants d\'un concours, trier les articles ou documents selon des critères analysés. Enfin ce qui vous plaira…',
	'evaluations_nom'         => 'Évaluations',
	'evaluations_slogan'      => 'Pour gérer et utiliser des grilles de notations avec commentaires',
);

?>
